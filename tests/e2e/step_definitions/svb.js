given(/^er naar de SVB ([^"]*) in de taal ([^"]*) wordt genavigeerd$/, (regeling, taal) => {
  switch (taal) {
    case 'Nederlands':
      if (regeling === 'Gemoedsbezwaren homepagina') {
        cy.visit('/nl/testen/')
      } else if (regeling === 'homepagina') {
        cy.visit('/nl/')
      }
      break;
  }
})

then('There is a h1 present on the page', () => {
  cy.get('h1').should('be.visible');
})
