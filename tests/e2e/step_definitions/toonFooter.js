import { Footer } from '../page_objects/Footer.po.js';

const footer = new Footer();

then(/^is de footer aanwezig$/, () => {
    cy.get(footer.paginaFooter)
    .should('be.visible');
})

then(/^zijn de volgende items aanwezig in de footer:$/, dataTable => {
    for (const item of dataTable.hashes()) {
        cy.get(footer.footerLink(item.ITEM))
        .should('be.visible');
    }
})

then(/^is de ([^"]*) linksectie aanwezig in de footer$/, linksectie => {
    cy.get(footer.footerLinksectie(linksectie))
    .should('be.visible');
})