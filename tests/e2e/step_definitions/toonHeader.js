import { Header } from '../page_objects/Header.po.js';

const header = new Header();

then(/^is de navigatie header voor (desktop|mobiel) (zichtbaar|niet zichtbaar)$/, (device, visibility) => {
  switch (visibility) {
    case 'zichtbaar':
      cy.get(header.navigatieHeader)
      .should('be.visible');
      break;
    case 'niet zichtbaar':
      cy.get(header.navigatieHeader)
      .should('be.not.visible');
      break;
  }
})

when(/^er naar beneden wordt gescrolled$/, () => {
  cy.scrollTo('bottom');
})

then(/^zijn er items voor het menu, zoeken en taalselectie zichtbaar$/, () => {
  cy.get(header.menuButtonHamburger)
  .should('be.visible');
  cy.get(header.navigatieHeader)
  .should('be.visible');
  cy.get(header.zoekButton)
  .should('be.visible');
  cy.get(header.zoekInvoerveld)
  .should('be.visible');
})
