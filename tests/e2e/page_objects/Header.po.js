export class Header {
    constructor(){
        this.navigatieHeader = '#navigation';
        this.menuButtonHamburger = 'button.btn-hamburger[data-toggle="menu"]:not(.responsive-burger)';
        this.zoekButton = '#search button[type="submit"]';
        this.zoekInvoerveld = 'form#search[method="get"]';
        this.taalSelectButton = '#language-select';
    }
}
