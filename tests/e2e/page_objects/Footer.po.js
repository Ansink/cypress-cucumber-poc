export class Footer {
    constructor(){
        this.paginaFooter = '#footer';
    }

    footerLink(linkText){
        return "div.footer-links span:contains("+linkText+")";
    }

    footerLinksectie(linkSectie){
        return "div.footer-links h4:contains("+linkSectie+")";
    }
}