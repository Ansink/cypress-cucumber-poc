# language: nl

Functionaliteit: The SVB Homepage
  Alls PO I want to visit the SVB homepage

Achtergrond: de homepage is geladen
    Gegeven er naar de SVB homepagina in de taal Nederlands wordt genavigeerd

  Scenario: Opening the SVB homepage
    Dan I see "SVB" in the title

  Scenario: There is a h1 present on the SVB homepage
    Dan There is a h1 present on the page
