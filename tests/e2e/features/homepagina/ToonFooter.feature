Feature: Homepagina - Toon footer
  Alls PO wil ik dat de nieuwe pagina's worden voorzien van de nieuw ontwikkelde footer,
  zodat gebruik kan worden gemaakt van de functionaliteit in deze footer
  en de nieuwe lay-out/stijl wordt toegepast.

  Background: de homepage van de regeling is geladen
    Given er naar de SVB Gemoedsbezwaren homepagina in de taal Nederlands wordt genavigeerd

  Scenario: De footer is zichtbaar op de landingspagina
    Then is de footer aanwezig

  Scenario: De footer bevat de juiste links
    Then zijn de volgende items aanwezig in de footer:
      | KOP                         | ITEM            |
      | Volg ons                    | Facebook        |
      | Volg ons                    | LinkedIn        |
      | Volg ons                    | Twitter         |
      | Service                     | Contact         |
      | Service                     | Help            |
      | De Sociale Verzekeringsbank | SVB Home        |
      | De Sociale Verzekeringsbank | Werken bij      |
      | De Sociale Verzekeringsbank | Over de SVB     |
      | De Sociale Verzekeringsbank | Servicegarantie |
      | De Sociale Verzekeringsbank | Pers            |

  Scenario Outline: De footer bevat de juiste link secties
    Then is de <linksectie> linksectie aanwezig in de footer
    Examples:
      | linksectie                  |
      | Volg ons                    |
      | Service                     |
      | De Sociale Verzekeringsbank |



