  # language: nl

  Functionaliteit: Homepagina - Toon header
  Alss PO wil ik dat de nieuwe pagina's worden voorzien van de nieuw ontwikkelde header,
  zodat gebruik kan worden gemaakt van de functionaliteit in deze footer
  en de nieuwe lay-out/stijl wordt toegepast.

  Achtergrond: de homepage is geladen
    Gegeven er naar de SVB Gemoedsbezwaren homepagina in de taal Nederlands wordt genavigeerd

  Scenario: Op mobiel wordt de header correct getoond
    Als de schermgrootte is mobiel
    Dan is de navigatie header voor mobiel zichtbaar
    Als er naar beneden wordt gescrolled
    Dan is de navigatie header voor mobiel niet zichtbaar

  Scenario: Op de desktop wordt de header correct getoond
    Als de schermgrootte is desktop
    Dan is de navigatie header voor desktop zichtbaar
    Als er naar beneden wordt gescrolled
    Dan is de navigatie header voor desktop zichtbaar
    En zijn er items voor het menu, zoeken en taalselectie zichtbaar

  # Scenario: header check
  #   Dan is de header voor de desktop zichtbaar
  #   En is de taalswitch zichtbaar
  #   En de taalswitch toont de huidige taal Nederlands
  
  # Scenario: header logo zichtbaar
  #   Dan is de header voor de desktop zichtbaar
  #   En is het SVB logo zichtbaar
